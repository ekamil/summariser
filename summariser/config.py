from pydantic import BaseSettings


class Settings(BaseSettings):
    mongo_username: str
    mongo_password: str
    mongo_host: str = "mongo"


def get_settings() -> Settings:
    return Settings()
