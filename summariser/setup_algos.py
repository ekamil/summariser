# Setup script to use in Dockerfile

if __name__ == "__main__":
    from summariser.text.algos import NLTK

    NLTK().setup()
