from fastapi import Depends
from pymongo.database import Database

from .database import get_db
from .text import ISummariseAlgo


async def is_db_healthy(db: Database = Depends(get_db)) -> bool:
    try:
        db.list_collections()
        healthy = True
    except:
        healthy = False
    return healthy


async def is_summariser_healthy(summarise: ISummariseAlgo) -> bool:
    try:
        summarise("Smoke test")
        healthy = True
    except:
        healthy = False
    return healthy
