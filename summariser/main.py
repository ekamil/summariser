import urllib.parse
from http import HTTPStatus
from logging import getLogger

from fastapi import FastAPI, Form, HTTPException, Path, Depends, BackgroundTasks
from pymongo.database import Database

from .database import get_db, get_collection
from .models import Document
from .repositories import DocumentRepository
from .schema import (
    HealthCheck,
    CreateSummaryResponse,
    DocumentResponse,
    DocumentId,
    SummaryResponse,
    Components,
)
from .text import NLTK, ISummariseAlgo
from .health_checks import is_db_healthy, is_summariser_healthy

logger = getLogger(__name__)

app = FastAPI()


def get_summarise() -> ISummariseAlgo:
    return NLTK()


def get_repository(db: Database = Depends(get_db)) -> DocumentRepository:
    return DocumentRepository(get_collection(db))


async def summarise_in_background(
    repo: DocumentRepository,
    summarise: ISummariseAlgo,
    document_id: DocumentId,
) -> None:
    document = repo.get(document_id)
    if not document:
        logger.error(
            "Document not found for task",
            extra={"document_id": document.id},
        )
        return
    if document.summary is not None:
        logger.error(
            "Document already has a summary",
            extra={"document_id": document.id},
        )
        return
    summary = summarise(document.input)
    logger.error(
        "Generated summary",
        extra={"document_id": document.id},
    )
    document.summary = summary
    repo.put(document)


@app.get("/health")
async def health_check(
    db: Database = Depends(get_db),
    summarise: ISummariseAlgo = Depends(get_summarise),
) -> HealthCheck:
    details = {
        Components.DB: await is_db_healthy(db),
        Components.ALGO: await is_summariser_healthy(summarise),
    }
    healthy = all(list(details.values()))
    if healthy:
        return HealthCheck(status="UP", details=details)
    else:
        return HealthCheck(status="ERROR", details=details)


@app.post("/documents", status_code=HTTPStatus.CREATED)
async def create_summary_request(
    background_tasks: BackgroundTasks,
    text: str = Form(...),
    repo: DocumentRepository = Depends(get_repository),
    summarise: ISummariseAlgo = Depends(get_summarise),
) -> CreateSummaryResponse:
    if not text:
        raise HTTPException(status_code=HTTPStatus.BAD_REQUEST)
    decoded = urllib.parse.unquote_plus(text)
    document_id = repo.put(
        Document(
            id=None,
            input=decoded,
            summary=None,
            input_length=len(decoded),
        )
    )
    background_tasks.add_task(
        summarise_in_background,
        repo=repo,
        summarise=summarise,
        document_id=document_id,
    )
    return CreateSummaryResponse(
        document_id=document_id,
        input_length=len(decoded),
    )


@app.get("/documents/{document_id}")
async def get_document(
    document_id: str = Path(...),
    repo: DocumentRepository = Depends(get_repository),
) -> DocumentResponse:
    document = repo.get(DocumentId(document_id))
    if document:
        return DocumentResponse(
            document_id=DocumentId(document.id),
            input=document.input,
            input_length=document.input_length,
        )
    else:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND)


@app.get("/summaries/{document_id}")
async def get_document_summary(
    document_id: str = Path(...),
    repo: DocumentRepository = Depends(get_repository),
) -> SummaryResponse:
    document = repo.get(DocumentId(document_id))
    if not document:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail="document")
    if document.summary is None:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail="document.summary")
    return SummaryResponse(
        document_id=DocumentId(document.id),
        summary=document.summary,
    )
