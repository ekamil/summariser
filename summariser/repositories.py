from logging import getLogger
from typing import Optional

from pymongo.collection import Collection

from summariser.models import Document
from summariser.schema import DocumentId, generate_uuid

logger = getLogger(__name__)


class DocumentRepository:
    def __init__(self, collection: Collection):
        self.collection = collection

    def put(self, document: Document) -> DocumentId:
        if document.id:
            update_result = self.collection.update_one(
                filter={"id": document.id},
                update={"$set": document.dict()},
                upsert=True,
            )
            logger.info(
                "Updated document %s", update_result, extra={"document_id": document.id}
            )
            return DocumentId(document.id)
        else:
            document.id = generate_uuid()
            insert_result = self.collection.insert_one(document.dict())
            logger.info(
                "Inserted new document %s",
                insert_result,
                extra={"document_id": document.id},
            )
            return DocumentId(document.id)

    def get(self, document_id: DocumentId) -> Optional[Document]:
        db_doc = self.collection.find_one(filter={"id": document_id})
        if db_doc:
            return Document(**db_doc)
        else:
            logger.debug(
                "Document not found",
                extra={"document_id": document_id},
            )
        return None

    def __contains__(self, document_id: DocumentId) -> bool:
        return self.collection.count_documents(filter={"id": document_id}) == 1
