import abc
from collections import defaultdict
from typing import Optional, Dict, Any

import nltk
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize, sent_tokenize


class ISummariseAlgo(abc.ABC):
    @abc.abstractmethod
    def __call__(self, input: str) -> Optional[str]:
        pass

    @abc.abstractmethod
    def setup(self) -> None:
        pass


class NLTK(ISummariseAlgo):
    threshold_multiplier = 1.3
    sentence_cutoff = 10
    language = "english"

    def setup(self):
        nltk.download("stopwords")
        nltk.download("punkt")

    def __call__(self, input: str) -> Optional[str]:
        frequency_table = self._create_frequency_table(input)
        sentences: Any = sent_tokenize(input)
        sentence_scores = self._score_sentences(sentences, frequency_table)
        threshold = self._find_average_score(sentence_scores)
        summary = self._generate_summary(
            sentences,
            sentence_scores,
            threshold,
        )

        return summary

    def _create_frequency_table(self, text: str) -> Dict[str, int]:
        stop_words = set(stopwords.words(self.language))
        words = word_tokenize(text)
        ps = PorterStemmer()
        freq_table = defaultdict(int)
        for word in words:
            word = ps.stem(word)
            if word in stop_words:
                continue
            freq_table[word] += 1
        return freq_table

    def _score_sentences(
        self,
        sentences,
        frequency_table: Dict[str, int],
    ) -> Dict[str, float]:
        sentence_value = dict()
        for sentence in sentences:
            word_count_in_sentence_except_stop_words = 0
            important_part: str = sentence[: self.sentence_cutoff]
            for word_value in frequency_table:
                if word_value in sentence.lower():
                    word_count_in_sentence_except_stop_words += 1
                    if important_part in sentence_value:
                        sentence_value[important_part] += frequency_table[word_value]
                    else:
                        sentence_value[important_part] = frequency_table[word_value]
            if important_part in sentence_value:
                sentence_value[important_part] = (
                    sentence_value[important_part]
                    / word_count_in_sentence_except_stop_words
                )
        return sentence_value

    def _find_average_score(self, sentence_scores: Dict[str, float]) -> float:
        return sum(sentence_scores.values()) / len(sentence_scores)

    def _generate_summary(self, sentences, sentence_scores, threshold) -> str:
        threshold = threshold * self.threshold_multiplier
        sentence_count = 0
        summaries = []
        for sentence in sentences:
            important_part: str = sentence[: self.sentence_cutoff]
            score = sentence_scores.get(important_part, 0)
            if score >= threshold:
                summaries.append(sentence)
                sentence_count += 1
        return " ".join(summaries)
