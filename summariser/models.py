from typing import Optional

import pydantic


class Document(pydantic.BaseModel):
    id: Optional[str]
    input: str
    input_length: int
    summary: Optional[str]
