from fastapi import Depends
from pymongo import MongoClient
from pymongo.collection import Collection
from pymongo.database import Database

from summariser.config import Settings, get_settings

DB_NAME = "summariser"
COLLECTION_NAME = "summariser"


def get_db(settings: Settings = Depends(get_settings)) -> Database:
    connection_string = f"mongodb://{settings.mongo_username}:{settings.mongo_password}@{settings.mongo_host}"
    try:
        client = MongoClient(connection_string)
        database = client.get_database(DB_NAME)
        yield database
    finally:
        client.close()


def get_collection(db: Database) -> Collection:
    collection = db.get_collection(COLLECTION_NAME)
    collection.create_index("id", unique=True)
    return collection
