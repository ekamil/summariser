import enum
import uuid
from typing import Union, Literal, NewType, Dict

import pydantic


class Components(enum.Enum):
    DB = "DB"
    ALGO = "ALGO"


class HealthCheck(pydantic.BaseModel):
    status: Union[
        Literal["UP"],
        Literal["ERROR"],
    ]
    details: Dict[Components, bool]


DocumentId = NewType("DocumentId", str)


def generate_uuid() -> str:
    return str(uuid.uuid4())


class CreateSummaryResponse(pydantic.BaseModel):
    document_id: DocumentId
    input_length: int


class DocumentResponse(pydantic.BaseModel):
    document_id: DocumentId
    input_length: int
    input: str


class SummaryResponse(pydantic.BaseModel):
    document_id: DocumentId
    summary: str
