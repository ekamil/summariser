# Summariser
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Environment setup

Project runs on Python 3.9 with `pytest` as a testrunner.

### Run in docker
`docker-compose -f test.docker-compose.yml run unit`

`docker-compose -f test.docker-compose.yml run integration`

`docker-compose -f test.docker-compose.yml down --rmi local`

### Run int tests locally
Integration tests in docker
```shell
export MONGO_USERNAME=admin
export MONGO_PASSWORD=`pwgen`
export MONGO_PORT=27888

docker run -d \
  --name mongo-on-docker \
  -p ${MONGO_PORT}:27017 \
  -e MONGO_INITDB_ROOT_USERNAME=${MONGO_USERNAME} \
  -e MONGO_INITDB_ROOT_PASSWORD=${MONGO_PASSWORD} \
  mongo

export MONGO_HOST="localhost:${MONGO_PORT}"
pipenv run pytest tests

# docker stop mongo-on-docker
# docker rm mongo-on-docker
```


## Running the api
```shell
docker-compose down --rmi local
docker-compose up
```

### Curl
```shell
curl -X GET http://localhost:8080/health
# {"status":"UP","details":{"DB":true,"ALGO":true}}
curl -X POST http://localhost:8080/documents --form-string text="Long text this is"
# {"document_id":"1c127889-81a7-45bf-8f75-f4ffce04a1c6","input_length":17}
curl -X GET http://localhost:8080/documents/1c127889-81a7-45bf-8f75-f4ffce04a1c6
# {"document_id":"1c127889-81a7-45bf-8f75-f4ffce04a1c6","input_length":17,"input":"Long text this is"}
curl -X GET http://localhost:8080/summaries/1c127889-81a7-45bf-8f75-f4ffce04a1c6
# {"document_id":"1c127889-81a7-45bf-8f75-f4ffce04a1c6","summary":""}
```

