FROM python:3.9.7

WORKDIR /code

RUN pip install --no-cache-dir --upgrade pipenv

COPY ./Pipfile /code/Pipfile
COPY ./Pipfile.lock /code/Pipfile.lock

RUN pipenv sync

COPY ./summariser /code/summariser

RUN pipenv run python -m 'summariser.setup_algos'

EXPOSE 80

CMD ["pipenv", "run", "uvicorn", "summariser.main:app", "--host", "0.0.0.0", "--port", "80"]
