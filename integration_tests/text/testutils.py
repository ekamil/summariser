import os
from collections import namedtuple

TextAndSummary = namedtuple("TextAndSummary", ["text", "summary"])


def read_text(filename: str) -> TextAndSummary:
    parent = os.path.dirname(os.path.realpath(__file__))
    input = os.path.join(parent, "inputs", filename)
    output = os.path.join(parent, "expected_outputs", filename)
    with open(input, "r") as ifd, open(output, "r") as ofd:
        return TextAndSummary(
            ifd.read(),
            ofd.read(),
        )
