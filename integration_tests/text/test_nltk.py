from typing import Callable

import pytest

from integration_tests.text.testutils import TextAndSummary, read_text
from summariser.text.algos import NLTK


@pytest.fixture(scope="session")
def summariser_algo() -> Callable[[str], str]:
    algo = NLTK()
    algo.setup()
    return algo


@pytest.fixture(scope="session")
def input_output() -> TextAndSummary:
    return read_text("nltk_1.txt")


def test_nltk_summarise(summariser_algo, input_output: TextAndSummary):
    assert input_output.summary == summariser_algo(input_output.text)
