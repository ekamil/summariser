import pytest
from pymongo.database import Database
from starlette.applications import Starlette
from starlette.testclient import TestClient

from summariser.config import get_settings
from summariser.database import get_db, COLLECTION_NAME
from summariser.main import get_repository
from summariser.repositories import DocumentRepository


@pytest.fixture
def app_under_test() -> Starlette:
    from summariser.main import app

    app.debug = True
    return app


@pytest.fixture
def test_api_client(app_under_test) -> TestClient:
    return TestClient(app_under_test)


@pytest.fixture
def repository() -> DocumentRepository:
    settings = get_settings()
    db: Database = next(get_db(settings))
    repository = get_repository(db)
    yield repository
    db.drop_collection(COLLECTION_NAME)
