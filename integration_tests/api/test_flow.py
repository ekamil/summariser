import time
from http import HTTPStatus
from unittest import mock

from fastapi.testclient import TestClient

from summariser.repositories import DocumentRepository
from summariser.schema import DocumentId, generate_uuid


def test_put_document_and_get_summary(
    test_api_client: TestClient,
    repository: DocumentRepository,
):
    text_to_summarise = "This is a long text "
    document_id = _post_text(test_api_client, text_to_summarise)
    doc_from_api: dict = _assert_can_get_document(test_api_client, document_id)
    assert doc_from_api["input"] == text_to_summarise
    time.sleep(1)  # hack
    _assert_can_get_summary(test_api_client, document_id)


def _assert_can_get_summary(test_api_client: TestClient, document_id: DocumentId):
    response = test_api_client.get(
        url=f"/summaries/{document_id}",
    )
    assert response.status_code == HTTPStatus.OK
    assert response.json() == {
        "document_id": document_id,
        "summary": mock.ANY,
    }


def _post_text(test_api_client: TestClient, text_to_summarise: str) -> DocumentId:
    response = test_api_client.post(
        url="/documents",
        headers={
            "Content-Type": "application/x-www-form-urlencoded",
        },
        data=f"text={text_to_summarise}",
    )
    assert response.status_code == HTTPStatus.CREATED
    assert "document_id" in response.json()
    document_id = response.json()["document_id"]
    return DocumentId(document_id)


def _assert_can_get_document(
    test_api_client: TestClient, document_id: DocumentId
) -> dict:
    response = test_api_client.get(
        url=f"/documents/{document_id}",
    )
    assert response.status_code == HTTPStatus.OK
    assert "document_id" in response.json()
    assert document_id == response.json()["document_id"]
    return response.json()


def test_document_not_found(
    test_api_client: TestClient, repository: DocumentRepository
):
    response = test_api_client.get(
        url=f"/summaries/{generate_uuid()}",
    )
    assert response.status_code == HTTPStatus.NOT_FOUND
    assert response.json() == {
        "detail": "document",
    }
