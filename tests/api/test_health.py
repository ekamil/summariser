from fastapi.testclient import TestClient


def test_health(test_api_client: TestClient):
    response = test_api_client.get("/health")
    assert response.status_code == 200
    assert response.json() == {"details": {"ALGO": True, "DB": True}, "status": "UP"}
