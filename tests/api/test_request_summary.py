from http import HTTPStatus
from unittest import mock

import pytest
from fastapi.testclient import TestClient

from summariser.models import Document


def test_valid_request_to_summarise(test_api_client: TestClient):
    text_to_summarise = "This+is+a+long+text%20"
    response = test_api_client.post(
        url="/documents",
        headers={
            "Content-Type": "application/x-www-form-urlencoded",
        },
        data=f"text={text_to_summarise}",
    )
    assert response.status_code == HTTPStatus.CREATED
    assert response.json() == {
        "document_id": mock.ANY,
        "input_length": len(text_to_summarise) - 2,  # space is decoded to a single char
    }


def test_can_get_a_document(
    test_api_client: TestClient, document_without_summary: Document
):
    response = test_api_client.get(
        url=f"/documents/{document_without_summary.id}",
    )
    assert response.status_code == HTTPStatus.OK
    assert response.json() == {
        "document_id": document_without_summary.id,
        "input": document_without_summary.input,
        "input_length": 14,
    }


def test_get_document_doesnt_exist(
    test_api_client: TestClient, missing_document: Document
):
    response = test_api_client.get(
        url=f"/documents/{missing_document.id}",
    )
    assert response.status_code == HTTPStatus.NOT_FOUND


@pytest.mark.parametrize("data", ["foo=1", None, " "])
def test_bad_request(test_api_client: TestClient, data):
    response = test_api_client.post(
        url="/documents",
        headers={
            "Content-Type": "application/x-www-form-urlencoded",
        },
        data=data,
    )
    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
