from http import HTTPStatus

from fastapi.testclient import TestClient

from summariser.models import Document


def test_valid_summary(
    test_api_client: TestClient,
    document_with_summary: Document,
):
    assert document_with_summary.summary
    response = test_api_client.get(
        url=f"/summaries/{document_with_summary.id}",
    )
    assert response.status_code == HTTPStatus.OK
    assert response.json() == {
        "document_id": document_with_summary.id,
        "summary": document_with_summary.summary,
    }


def test_document_not_found(
    test_api_client: TestClient,
    missing_document: Document,
):
    response = test_api_client.get(
        url=f"/summaries/{missing_document.id}",
    )
    assert response.status_code == HTTPStatus.NOT_FOUND
    assert response.json() == {
        "detail": "document",
    }


def test_document_found_but_without_summary(
    test_api_client: TestClient,
    document_without_summary: Document,
):
    assert document_without_summary.summary is None
    response = test_api_client.get(
        url=f"/summaries/{document_without_summary.id}",
    )
    assert response.status_code == HTTPStatus.NOT_FOUND
    assert response.json() == {
        "detail": "document.summary",
    }
