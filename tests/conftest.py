from typing import Optional
from unittest.mock import MagicMock

import pytest
from starlette.applications import Starlette
from starlette.testclient import TestClient

from summariser.config import Settings, get_settings
from summariser.database import get_db
from summariser.main import get_repository, get_summarise
from summariser.models import Document
from summariser.repositories import DocumentRepository
from summariser.schema import generate_uuid, DocumentId
from summariser.text import ISummariseAlgo


def get_settings_override():
    return Settings(mongo_host="mongo", mongo_password="empty", mongo_username="empty")


class MockDocumentRepo(DocumentRepository):
    def __init__(self):
        self.storage = {}

    def put(self, document: Document) -> DocumentId:
        doc_id = document.id or generate_uuid()
        self.storage[doc_id] = document
        return DocumentId(doc_id)

    def get(self, document_id: DocumentId) -> Optional[Document]:
        return self.storage.get(document_id)

    def __contains__(self, document_id: DocumentId) -> bool:
        return document_id in self.storage


@pytest.fixture
def in_memory_repository() -> DocumentRepository:
    return MockDocumentRepo()


def get_db_override() -> MagicMock:
    mock_database = MagicMock()
    return mock_database


class NoopSummarise(ISummariseAlgo):
    def __call__(self, input: str) -> Optional[str]:
        return "empty"

    def setup(self) -> None:
        pass


def get_summarise_override() -> ISummariseAlgo:
    return NoopSummarise()


@pytest.fixture
def app_under_test(in_memory_repository: DocumentRepository) -> Starlette:
    from summariser.main import app

    app.dependency_overrides[get_settings] = get_settings_override
    app.dependency_overrides[get_db] = get_db_override
    app.dependency_overrides[get_repository] = lambda: in_memory_repository
    app.dependency_overrides[get_summarise] = get_summarise_override
    app.debug = True
    return app


@pytest.fixture
def test_api_client(app_under_test) -> TestClient:
    return TestClient(app_under_test)


@pytest.fixture
def missing_document(in_memory_repository: DocumentRepository) -> Document:
    doc = Document(
        id=generate_uuid(),
        input="Very long text",
        input_length=14,
        summary=None,
    )
    return doc


@pytest.fixture
def document_without_summary(in_memory_repository: DocumentRepository) -> Document:
    doc = Document(
        id=generate_uuid(),
        input="Very long text",
        input_length=14,
        summary=None,
    )
    in_memory_repository.put(doc)
    return doc


@pytest.fixture
def document_with_summary(in_memory_repository: DocumentRepository) -> Document:
    doc = Document(
        id=generate_uuid(),
        input="Different very long text",
        input_length=24,
        summary="Different very long text",
    )
    in_memory_repository.put(doc)
    return doc
