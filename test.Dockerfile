FROM python:3.9.7

WORKDIR /code

RUN pip install --no-cache-dir --upgrade pipenv

COPY ./Pipfile /code/Pipfile
COPY ./Pipfile.lock /code/Pipfile.lock

RUN pipenv sync --pre --dev

COPY ./summariser /code/summariser
COPY ./tests /code/tests
COPY ./integration_tests /code/integration_tests

